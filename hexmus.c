#if defined(__MSDOS__)
#include <conio.h>
#include <dos.h>
#elif defined(__gnu_linux__)
#include <sys/ioctl.h>
#include <unistd.h>
#include <linux/kd.h>
const char *colors[8] =
  { "\x1B[30m",
    "\x1B[31m",
    "\x1B[32m",
    "\x1B[33m",
    "\x1B[34m",
    "\x1B[35m",
    "\x1B[36m",
    "\x1B[37m" };
const char *reset = "\x1B[0m";
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "inih/ini.h"

typedef struct
{
  int tempo;
  int notes[15];
} configuration;

static int handler(void* user, const char* section, const char* name,
		   const char* value)
{
  int match;
  int i;
  char str[12];
  configuration* pconfig = (configuration*)user;

#define MATCH(s, n) strcmp(section, s) == 0 && strcmp(name, n) == 0
  if (MATCH("output", "tempo")) {
    pconfig->tempo = atoi(value);
  } else {
    match = 0;
    for (i = 0; i < 15; i++) {
      sprintf(str, "notes[%d]", i);
      if (MATCH("output", str)) {
	pconfig->notes[i] = atoi(value);
	match = 1;
      }
    }
    if (!match) {
      return 0; /* unknown section/name, error */
    }
  }
  return 1;
}

/* C major, C3 to C5 */
const int default_notes[15] = {130, 146, 164, 174, 196, 220, 246, 261, 293, 329, 349, 392, 440, 493, 523};

void help() {
  printf("usage: hexmus input_file [config_file]\n");
}

int main(int argc, const char *argv[]) {
  configuration config;
  int i, note, duration;
  /* default config */
  config.tempo = 120;
  for(i = 0; i < 15; i++) {
    config.notes[i] = default_notes[i];
  }
  if (argc > 1) {
    FILE* fp;
#if defined(__MSDOS__)
    if (strcmp(argv[1], "/?") == 0) {
#elif defined(__gnu_linux__)
    if (strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "--help") == 0 ) {
#endif
      help();
      return 0;
    }
    if (argc > 2) {
      int parse_result = ini_parse(argv[2], handler, &config);
      if (parse_result < 0) {
	printf("Config file %s not found.\n", argv[2]);
	return 1;
      } else if (parse_result > 0) {
	printf("Error parsing config file.\n");
	return 1;
      }
    }
    fp = fopen(argv[1], "rb");
    if (fp) {
      int c;
#if defined(__MSDOS__)
      textmode(3); /* C80 */
      textbackground(0);
#endif	
      while ((c = fgetc(fp)) != EOF) {
	note = c / 16;
	duration = c % 16 + 1;
#if defined(__MSDOS__)
	if (note == 0) {
	  nosound();
	} else {
	  sound(config.notes[note - 1]);
	}
	textcolor(note);
	cprintf("%02X ", c);
	/* whole note in 120 BMP should have delay of 2000 ms */
	delay((int)((float)duration * 15000 / config.tempo));
#elif defined(__gnu_linux__)
	if (note == 0) {
	  ioctl(STDOUT_FILENO, KIOCSOUND, 0);
	} else {
	  ioctl(STDOUT_FILENO, KIOCSOUND, 1193180/config.notes[note-1]);
	}
	if (note>8) {
	  printf("\x1B[40m"); /* black background */
	}
	printf("%s%02X%s ", colors[note/2], c, reset);
	fflush(stdout);
	usleep((int)((double)duration * 15000000 / config.tempo));
#endif	
      }
      fclose(fp);
    } else {
      printf("Input file %s not found.\n", argv[1]);
    }
  } else {
    help();
    return 0;
    }
#if defined(__MSDOS__)
  nosound();
  textcolor(7); /* light gray */
#elif defined(__gnu_linux__)
  ioctl(STDOUT_FILENO, KIOCSOUND, 0);
  printf("\n");
#endif
  return 0;
}
