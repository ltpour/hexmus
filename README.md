# HEXMUS #

## Summary ##

An experimental tool for converting binary or text files to music. The first hex in each byte is interpreted as one of 15 notes (or 0 for a pause), and the second hex as the duration of that note (from sixteenth note to a whole note).

I wrote this primarily for MS-DOS. It should work on GNU/Linux as well, but I haven't had the chance to test it as it only supports the PC speaker for audio output and none of my current systems have one. It does run on DOSBox, which is currently also the recommended method of capturing the audio as saving the output to a wave file hasn't been implemented yet in the program itself.

Compiling on DOS (or DOSBox) requires the proprietary CONIO and DOS libraries that come with the Borland series of compilers. I use Borland C++ 2.0, but the Turbo C/C++ compilers should work as well.

Here's a screenshot of hexmus playing back it's own source code on DOSBox:

![screenshot](scrot.png)

## Usage ##

The program takes two arguments, the input file and an optional configuration file. The example file `config.ini` contains the default settings.

## Installation ##

### MS-DOS ###

First compile the inih library:

`cd inih`
`copy Makefile.DOS Makefile`
`make`
`cd ..`

The compile and install hexmus itself:
 
`copy Makefile.DOS Makefile`
`make`
`make install`

Make sure that C:\BIN is in your PATH, or edit the DESTDIR variable in the makefile to point to a path that is.

### GNU/Linux ###

First compile the inih library:

`cd inih`
`copy Makefile.GNU Makefile`
`make`
`cd ..`

The compile and install hexmus itself:
 
`cp Makefile.GNU Makefile`
`make`
`make install`

## TODO ##

- saving output to wave file
- more customization options
- nicer formatting
